def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num = 2)
  mod = string + " "
  (mod * num).strip

end

def start_of_word(word, num)
  arr = word.split("")
  arr.shift(num).join("")
end


def first_word(string)
  string.split(" ").to_a.first
end

def titleize(str)
    str.capitalize!
    words_no_cap = ["and", "or", "the", "over", "to", "the", "a", "but"]
    final = str.split(" ").map {|word|
        if words_no_cap.include?(word)
            word
        else
            word.capitalize
        end
    }.join(" ")
  final
end
