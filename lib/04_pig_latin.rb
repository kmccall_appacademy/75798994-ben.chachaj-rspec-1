# def translate(string)
#   words = string.split(" ").to_a
#   words.map! do |word|
#     add_ay(word)
#   end
#   p words.join(" ")
# end
#
# def add_ay(word)
#   alpha = ('a'..'z').to_a
#   vowels = %w[a e i o u]
#   consonants = alpha - vowels
#
#   if vowels.include?(word[0])
#     word + 'ay'
#   elsif consonants.include?(word[0]) && consonants.include?(word[1])
#     word[2..-1] + word[0..1] + 'ay'
#   elsif word[0] + word[1] + word[2] == "sch"
#     word[3..-1] + word[0..2] + "ay"
#   elsif word[0] + word[1]== "qu"
#     word[3..-1] + word[0..2] + "ay"
#   elsif consonants.include?(word[0])
#     word[1..-1] + word[0] + 'ay'
#   else
#     word # return unchanged
#   end
# end


def translate(string)
array = string.split(" ")
alphabet = ("a".."z").to_a
vowels = ["a", "e", "i", "o", "u"]
options = alphabet - vowels
options += ["qu", "sch", "thr"]
result = []
array.each do |word|
    if vowels.include?(word[0])
        result << (word + "ay")
    elsif options.include?(word[0..1])
        result << (word[2..-1] + (word[0..1] + "ay"))
    elsif options.include?(word[0]) && "q".include?(word[1])  && "u".include?(word[2])
          result << (word[3..-1] + (word[0..2] + "ay"))
    elsif options.include?(word[0]) && options.include?(word[1]) && options.include?(word[2])
        result << (word[3..-1] + (word[0..2] + "ay"))
    elsif options.include?(word[0]) && options.include?(word[1])
        result << (word[2..-1] + (word[0..1] + "ay"))
    elsif options.include?(word[0]) && "q".include?(word[1])  && "u".include?(word[2])
        result << result << (word[4..-1] + (word[0..3] + "ay"))
    elsif options.include?(word[0])
        result << (word[1..-1] + (word[0]+ "ay"))
    end
end
  result.join(" ")
end
