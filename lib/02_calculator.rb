def add(x, y)
  x + y
end


def subtract(x,y)
  x - y
end


def sum (array)
  array.length > 0 ? array.reduce(:+) : 0
end
